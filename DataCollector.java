package archMethodsProject;

import java.util.*;

//what do I want to collect?
// average adult lifespan
// average IBI value
// growth rate


public class DataCollector {
	
	private int numTimeSteps;
	
	private List<Double> avgLifespan;
	private List<Double> avgAgeAtDeath;
	private List<Double> avgIBI;
	private List<Double> avgReproPeriod;
	private List<Double> avgAgeFirstRepro;
	private List<Double> popSize;
	private List<Double> growthRate;
	private List<Double> birthRate;
	private List<Double> deathRate;
	
	//age structure?
	
	public DataCollector() {
		numTimeSteps = 0;
		
		avgLifespan = new ArrayList<Double>();
		avgAgeAtDeath = new ArrayList<Double>();
		avgIBI = new ArrayList<Double>();
		avgReproPeriod = new ArrayList<Double>();
		avgAgeFirstRepro = new ArrayList<Double>();
		popSize = new ArrayList<Double>();
		growthRate = new ArrayList<Double>();
		birthRate = new ArrayList<Double>();
		deathRate = new ArrayList<Double>();
		
	}
	
	public int getNumTimeSteps() { return numTimeSteps; }
	public void increaseTimeStep() { this.numTimeSteps++; }

	public List<Double> getAvgLifespan() { return avgLifespan; }
	public void addAvgLifespan(int index, double avgLifespan) { 
		this.avgLifespan.add(index, avgLifespan);
	}
	
	public List<Double> getAvgAgeAtDeath() { return avgAgeAtDeath; }
	public void addAvgAgeAtDeath(int index, double avgAgeAtDeath) { 
		this.avgAgeAtDeath.add(index, avgAgeAtDeath);
	}
	
	public List<Double> getAvgIBI() { return avgIBI; }
	public void addAvgIBI(int index, double avgIBI) {
		this.avgIBI.add(index, avgIBI);
	}
	
	public List<Double> getAvgReproPeriod() { return avgReproPeriod; }
	public void addAvgReproPeriod(int index, double avgReproPeriod) {
		this.avgReproPeriod.add(index, avgReproPeriod);
	}
	
	public List<Double> getAvgAgeFirstRepro() { return avgAgeFirstRepro; }
	public void addAvgAgeFirstRepro(int index, double avgAge) {
		this.avgAgeFirstRepro.add(index, avgAge);
	}
	
	public List<Double> getPopSize() { return popSize; }
	public void addPopSize(int index, double pop) {
		this.popSize.add(index, pop);
	}
	
	public List<Double> getGrowthRate() { return growthRate; }
	public void addGrowthRate(int index, double gr) {
		this.growthRate.add(index, gr);
	}
	
	public List<Double> getBirthRate() { return birthRate; }
	public void addBirthRate(int index, double br) {
		this.birthRate.add(index, br);
	}
	
	public List<Double> getDeathRate() { return deathRate; }
	public void addDeathRate(int index, double dr) {
		this.deathRate.add(index, dr);
	}
	
	public String printDemography() {
		String output = "";
		for(int i = 0; i <= numTimeSteps; i++) {
			output += "STEP " + i;
			output += "\n \t Population size = " + popSize.get(i);
			output += "\n \t Growth rate = " + growthRate.get(i);
			output += "\n \t Average lifespan = " + avgLifespan.get(i);
			output += "\n \t Average age first reproduction = " + avgAgeFirstRepro.get(i);
			output += "\n \t Average reproductive period = " + avgReproPeriod.get(i);
			output += "\n \t Average IBI = " + avgIBI.get(i);
			output += "\n \t Average age at death = " + avgAgeAtDeath.get(i) + "\n";
		}
		
		return output;
	}
	
	
	

}
