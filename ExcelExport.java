package archMethodsProject;

import java.io.*;

public class ExcelExport {
	
	private static final String comma = ",";
	private static final String newline = "\n";
	private static final String headers = "Year,Pop_Size,Growth_Rate,Avg_Lifespan,"
			+ "Avg_AAD,Avg_IBI,Avg_RP,Avg_AFR,Birth_Rate,Death_Rate";
	
	public void export(String fileName, DataCollector dc, double timeStep) {
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(fileName);
			fw.append(headers.toString());
			fw.append(newline);
			
			for(int i = 0; i < dc.getNumTimeSteps(); i++ ){
				fw.append("" + i*timeStep);
				fw.append(comma);
				fw.append("" + dc.getPopSize().get(i));
				fw.append(comma);
				fw.append("" + dc.getGrowthRate().get(i));
				fw.append(comma);
				fw.append("" + dc.getAvgLifespan().get(i));
				fw.append(comma);
				fw.append("" + dc.getAvgAgeAtDeath().get(i));
				fw.append(comma);
				fw.append("" + dc.getAvgIBI().get(i));
				fw.append(comma);
				fw.append("" + dc.getAvgReproPeriod().get(i));
				fw.append(comma);
				fw.append("" + dc.getAvgAgeFirstRepro().get(i));
				fw.append(comma);
				fw.append("" + dc.getBirthRate().get(i));
				fw.append(comma);
				fw.append("" + dc.getDeathRate().get(i));
				fw.append(newline);
				
			}
			
		} catch (IOException e) {
			System.out.println("IOException");
			e.printStackTrace();
		} finally {
			try {
				fw.flush();
				fw.close();
			} catch (IOException e) {
				System.out.println("error while closing");
				e.printStackTrace();
			}
		}
		
	}

}
