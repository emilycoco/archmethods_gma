package archMethodsProject;

import java.util.*;

public class Individual {

	public World world;

	public static int nextID = 1;

	public int id;
	private Individual mother, father;
	private double age;
	private int sex; //male = 0, female = 1
	private boolean alive; // alive = true, dead = false
	private boolean fertile;
	private boolean nursing;

	private double ageAtDeath;
	private double ageAtFirstRepro;
	private double timeSinceLastBirth;
	private double IBI;
	private boolean firstBirth;

	private double ageIndependent;
	private double ageAtSexMature;
	private double ageAtEndFert;
	private double ageAtFrail;

	private double lifespan;
	private double reproPeriod;
	private double trp;

	private Individual currentMate;
	private List<Individual> children;
	//private int childCount;


	public Individual() {
		id = nextID++;

		age = 0;
		alive = true;
		fertile = false;
		nursing = false;
		firstBirth = true;

		currentMate = null;
		children = new ArrayList<Individual>();
		//childCount = 0;

	}

	//----------GETTERS/SETTERS/ADDERS-------------------------

	public void setWorld(World w) { this.world = w; }
	public void resertNextID() { nextID = 1; }
	public int getId() { return id; }

	public Individual getMother() { return mother; }
	public void setMother(Individual mother) { this.mother = mother; }

	public Individual getFather() { return father; }
	public void setFather(Individual father) { this.father = father; }

	public double getAge() { return age; }
	public void setAge(double age) { this.age = age; }

	public int getSex() { return sex; }
	public void setSex(int sex) { this.sex = sex;}

	public boolean isAlive() { return alive; }
	public void setAlive(boolean alive) { this.alive = alive; }

	public boolean isFertile() { return fertile; }
	public void setFertile(boolean fertile) { this.fertile = fertile;}

	public boolean isNursing() { return nursing; }
	public void setNursing(boolean nursing) { this.nursing = nursing;}

	public boolean getFirstBirth() { return firstBirth; }
	public void setFirstBirth(boolean fb) { this.firstBirth = fb; }

	public double getAgeAtDeath() { return ageAtDeath; }
	public void setAgeAtDeath(double ageAtDeath) { this.ageAtDeath = ageAtDeath; }

	public double getAgeAtIndependence() { return ageIndependent; }
	public void setAgeAtIndependence(double d) { this.ageIndependent = d; }

	public double getAgeAtSexMature() { return ageAtSexMature; }
	public void setAgeAtSexMature(double d) { this.ageAtSexMature = d; }

	public double getAgeAtFirstRepro() { return ageAtFirstRepro; }
	public void setAgeAtFirstRepro(double ageAtFirstRepro) { this.ageAtFirstRepro = ageAtFirstRepro;}

	public double getTimeSinceLastBirth() { return timeSinceLastBirth;}
	public void setTimeSinceLastBirth(double d) { this.timeSinceLastBirth = d; }

	public double getIBI() { return IBI; }
	public void setIBI(double d) { this.IBI = d; }

	public double getAgeAtEndFert() { return ageAtEndFert; }
	public void setAgeAtEndFert(double ageAtEndFert) { this.ageAtEndFert = ageAtEndFert; }

	public double getAgeAtFrail() { return ageAtFrail; }
	public void setAgeAtFrail(double age) { this.ageAtFrail = age; }

	public double getLifespan() { return lifespan; }
	public void setLifespan(double d) { this.lifespan = d; }

	public double getReproPeriod() { return reproPeriod; }
	public void setReproPeriod(double d) { this.reproPeriod = d; }

	public double getTRP() { return trp; }

	public Individual getCurrentMate() { return currentMate; }
	public void addMate(Individual mate) { this.currentMate = mate; }

	public List<Individual> getChildren() { return children; }
	public void addChild(Individual child) { this.children.add(child); }


	//-------------MODEL RUN METHODS--------------------

	public double fertility() { //Gage 2001, used by Kachel et al 
		double fert = 0;
		double fertLevel = world.fertLevel;
		if(world.toggleDynamic) {
			fertLevel = (8.0*12.0)/Math.pow(this.getReproPeriod(),4);
		}
		fert = fertLevel*(this.getAge() - this.getAgeAtSexMature())
				*(Math.pow(this.getAgeAtSexMature() + this.getReproPeriod() - this.getAge(), 2));
		return fert;
	}

	public double mortality() { 
		//Siler model, Kachel et al
		double comp1 = world.a1*Math.pow(Math.E, (-1.0*world.b1)*this.getAge());
		double comp2 = world.a2;
		double comp3 = world.a3*Math.pow(Math.E, world.b3*this.getAge());
		double b3 = world.b3;
		if(world.toggleDynamic) {
			b3 = (1.0/this.getLifespan())*((Math.log(1.0-(world.a1*Math.exp(-1.0*world.b1*this.getLifespan())))-comp2)/world.a3);
		}
		comp3 = world.a3*Math.pow(Math.E, b3*this.getAge());
		
		double siler = comp1 + comp2 + comp3;
		return 1.0 - Math.exp(-1.0*siler); //mortality probability
	}

	public void incrementAge() {
		this.age += 0.5;

		if(this.age >= this.getAgeAtSexMature() && 
				this.age <= Math.min(2*this.getLifespan(), this.getAgeAtEndFert())) {
			this.setFertile(true);
			if(!this.firstBirth) {
				this.timeSinceLastBirth += 0.5;
			}
		}
		else {
			this.setFertile(false);
			this.timeSinceLastBirth = 0;
		}
	}

	public void updateAge() {
		Individual mother = this.getMother();
		if(mother != null && !mother.getChildren().isEmpty()) {
			for(Individual i : mother.getChildren()) {
				if(i.getId() == this.getId()) {
					i.setAge(this.getAge());
					mother.checkNursing();
				}
			}
		}
	}

	public boolean chooseMate() {	
		if(world.adultMales.isEmpty()) {
			world.updateMalesList(); //resets male list if all men have already reproduced once
		}
		double collectiveFert = world.collectiveMaleFert();
		double maxProb = 0;
		int location = -1;
		for(int i = 0; i < world.adultMales.size(); i++) {
			double maleProb = world.adultMales.get(i).fertility()/collectiveFert;
			if(maleProb > maxProb) {
				maxProb = maleProb;
				location = i;
			}
		}
		if(location != -1) {
			this.addMate(world.adultMales.get(location));
			return true;
		}
		else return false;
	}

	public void giveBirth() {
		if(this.getNumDependents() == 0) { //only one dependent at a time
			double birthCheck = Math.random();
			double currentFert = this.fertility();

			if(currentFert >= birthCheck ) {
				this.currentMate = null;
				if(this.chooseMate()){
					Individual child = new Individual();
					Individual mother = this;
					Individual father = mother.getCurrentMate();
					child.setMother(mother);
					child.setFather(father);

					double lifespan = Math.pow(mother.getLifespan() * father.getLifespan(),0.5);
					if(world.toggleMutations) {
						double lifespanCheck = Math.random();
						if( lifespanCheck <= 0.05) {
							double mutation = -5.0 + (5.0 - (-5.0)) * new Random().nextDouble();
							reproPeriod += mutation;
						}
					}
					child.setLifespan(lifespan);

					double reproPeriod = Math.pow(mother.getReproPeriod() * father.getReproPeriod(),0.5);
					if(world.toggleMutations) {
						double reproCheck = Math.random();
						if(reproCheck <= 0.05) {
							double mutation = -5.0 + (5.0 - (-5.0)) * new Random().nextDouble();
							reproPeriod += mutation;
						}
					}
					child.setReproPeriod(reproPeriod);

					int sex = generateRandomInt(0,1, new Random());
					child.setSex(sex);
					child.setAgeAtIndependence(child.getLifespan()/10.0);

					if(child.sex == 0) {
						child.setAgeAtSexMature(15); //from Kim et al and Kachel et al
						child.setAgeAtEndFert(Math.min(2*child.getLifespan(), 75));
					}
					else {
						child.setAgeAtSexMature((child.getLifespan()/4.0) + world.ageOfWeaning); //from Kim et al
						child.setAgeAtFrail(Math.min(2*child.getLifespan(), 75));
						child.setAgeAtEndFert(child.getAgeAtSexMature() + child.getReproPeriod());
					}

					world.newchildren.add(child);
					world.numBirths++;
					mother.children.add(child);

					if(mother.getFirstBirth()) {
						mother.setAgeAtFirstRepro(mother.getAge());
						mother.setFirstBirth(false);
					}
					mother.setIBI(mother.getTimeSinceLastBirth());
					mother.setTimeSinceLastBirth(0);
					father.children.add(child);
					this.setNursing(true);
					child.setWorld(this.world);

					if(world.toggleOutput) {
						world.addOutput("Mother: " + mother.id + " Father: " + father.id + " gave birth to Individual " + child.id);
					}
				}

			}
		}
	}


	private int generateRandomInt(int start, int end, Random random) {
		long range = (long)end - (long)start + 1;
		long fraction = (long)(range*random.nextDouble());
		int randomNum = (int)(fraction + start);
		return randomNum;
	}

	public int getNumDependents() {
		int count = 0;
		if(!this.children.isEmpty()) {
			for(Individual i : this.children) {
				if(i.age < i.ageIndependent && i.isAlive()) {
					count++;
				}
			}
		}
		return count;
	}

	public void checkNursing() {
		this.setNursing(false);
		if(this.getSex() == 1) {
			for(Individual c : this.getChildren()) {
				if(c.getAge() <= world.ageOfWeaning && c.isAlive()) {
					this.setNursing(true);
				}
			}
		}
	}

	public void checkDeath() {
		if(this.age >= 80.0) { //Siler model does not reproduce mortality well above 80
			this.die();
		}

		double randomNum = Math.random();
		double deathProb = this.mortality();
		if(deathProb > randomNum) {
			this.die();
		}
	}

	public void die() {
		this.setAlive(false);
		this.setAgeAtDeath(this.age);
		world.dead.add(this);
		world.numDeaths++;
		if(this.getMother() != null) {
			this.getMother().getChildren().remove(this);
		}
		if(world.toggleOutput) {
			world.addOutput("Individual " + this.id + " (age " + this.age + ") "+ " died : " + this.mortality());
		}

		if(this.sex == 1) {
			for(int j = 0; j < this.children.size(); j++) {
				Individual i = this.children.get(j);
				if(i.getAge() < i.getAgeAtIndependence() && i.isAlive()) {
					i.setAlive(false);
					i.setAgeAtDeath(i.age);
					world.dead.add(i);
					world.numDeaths++;
					children.remove(i);
					if(world.toggleOutput) {
						world.addOutput("\t child: Individual " + i.id + " died");
					}
				}
			}
		}
	}

	public void grandmother() {
		if(this.getNumDependents() == 0) { //can only grandmother one child at a time
			List<Individual> possibleDepends = new ArrayList<Individual>();
			for(Individual i : world.persons) {
				if(i.isAlive()) {
					if(i.getAge() > world.ageOfWeaning && i.getAge() < i.getAgeAtIndependence()) {
						possibleDepends.add(i);
					}
				}	
			}
			//pick random child to grandmother
			if(!possibleDepends.isEmpty()) {
				Individual pick = possibleDepends.get(new Random().nextInt(possibleDepends.size()));
				if(pick != null) {
					if(world.toggleOutput) {
						world.addOutput("Individual " + this.id + " is grandmother to individual " + pick.id);
					}
					this.addChild(pick);
					if(pick.mother != null) {
						pick.mother.children.remove(pick);
					}
					pick.setMother(this);
				}
			}
		}
	}

	public String printIndividual() {
		String output = "";
		output += "Individual " + this.getId() + ", age " + this.getAge();
		output += ", lifespan " + this.getLifespan();
		if(this.getSex() == 0) {
			output += ", male";
			if(this.isFertile()) {
				output += " FERTILE";
			}
		}
		else {
			output += ", female";
			if(this.isFertile()) {
				output += " FERTILE";
			}
			if(this.isNursing()) {
				output += " NURSING";
			}
		}
		return output;
	}


}
