package archMethodsProject;

import java.util.*;

public class TestDynamic {

	public static void main(String[] args) {
		for(int run = 1; run <= 10; run++) {
			World w = new World(500,500,true, true, true);
			w.toggleDynamic(); //makes fertility and mortality dynamic
			
			w.setInitialLifespan(29.44); //chimpanzee expected lifespan at age 0, Gage 1998
			w.setInitialRP(40.515);
			w.setFertLevel(0.0000276); //Gage 1998
			w.setSilerModelParameters(2.864, 21.311, 0.01669, 0.00005260, 0.1469); //chimpanzee values from Gage 1998
			w.setAgeDistributionPercentages(0.21, 0.133, 0.162, 0.114, 0.343, 0.038); //HH percentages
			
			w.initializePeople();
			System.out.println("begin run");
			
			DataCollector dc = new DataCollector();
			List<Double> initialData = w.collectLiveData();
			dc.addAvgLifespan(0, initialData.get(0));
			dc.addAvgAgeAtDeath(0, w.calculateAvgAgeAtDeath());
			dc.addAvgIBI(0, initialData.get(1));
			dc.addAvgReproPeriod(0, initialData.get(2));
			dc.addAvgAgeFirstRepro(0, initialData.get(3));
			dc.addPopSize(0, w.persons.size());
			dc.addGrowthRate(0, 0.0);
			dc.addBirthRate(0, 0.0);
			dc.addDeathRate(0, 0.0);
			
			//for(int i = 1; i <= 2000000; i++) { //simulation runs for 1mil years
			for(int i=1; i <= 200000; i ++) { //simulation runs for 100k years
				w.step();
				if(i % 200 == 0) { //data collection every 100 years
					dc.increaseTimeStep();
					List<Double> data = w.collectLiveData();
					dc.addAvgLifespan(dc.getNumTimeSteps(), data.get(0));
					dc.addAvgAgeAtDeath(dc.getNumTimeSteps(), w.calculateAvgAgeAtDeath());
					dc.addAvgIBI(dc.getNumTimeSteps(), data.get(1));
					dc.addAvgReproPeriod(dc.getNumTimeSteps(), data.get(2));
					dc.addAvgAgeFirstRepro(dc.getNumTimeSteps(), data.get(3));
					dc.addPopSize(dc.getNumTimeSteps(), w.persons.size());
//					int prevPop = pastPop.get(dc.getNumTimeSteps()-1);
//					double gr = (w.persons.size() - prevPop)/(prevPop * 200);
					dc.addGrowthRate(dc.getNumTimeSteps(), 0); //not calculating growth rate for this experiment
					dc.addDeathRate(dc.getNumTimeSteps(), 0);
					dc.addBirthRate(dc.getNumTimeSteps(), 0);
				}
			}
			
			//writing results file
			String wb = "/Users/emilycoco/Desktop/Model Data Output/Dynamic/";
			if(w.toggleDynamic) {
				wb += "Presence/dc" + run + "_d.csv";
			}
			else {
				wb += "Absence/dc" + run + "_nod.csv";
			}
			ExcelExport ee = new ExcelExport();
			ee.export(wb, dc, 100);

			System.out.println("end run");
			
		}
	}

}
