package archMethodsProject;

import java.util.*;

public class TestGrowthRate {

	public static void main(String[] args) {
		World[] worlds = {
				new World(500, 500, false, true, true), 
				new World(500, 500, true, true, true), 
				new World(500, 500, false, true, true), 
				new World(500, 500, true, true, true)
		};

		for(int n = 3; n < 4; n++) { //run between 0 and 2 for chimpanzee, run between 2 and 4 for human
			for(int run = 1; run <= 20; run++) {
				World w = worlds[n];
				if(n < 2) {
					w.setInitialLifespan(29.44); //chimpanzee expected lifespan at age 0, Gage 1998
					w.setInitialRP(40.515);
					w.setFertLevel(0.0000276); //Gage 1998
					w.setSilerModelParameters(2.864, 21.311, 0.01669, 0.00005260, 0.1469); //chimpanzee values from Gage 1998
					w.setAgeDistributionPercentages(0.21, 0.133, 0.162, 0.114, 0.343, 0.038);
				}
				else {
					w.setInitialLifespan(65.08);
					w.setInitialRP(35.947);
					w.setFertLevel(0.00004597);
					w.setSilerModelParameters(0.116, 1.237, 0.00111, 0.00003353, 0.0997);
					w.setAgeDistributionPercentages(0.25, 0.19, 0.16, 0.12, 0.24, 0.04); //Kachel et al
				}
				DataCollector dc = new DataCollector();

				w.initializePeople();
				System.out.println("begin run");

				List<Double> initialData = w.collectLiveData();
				dc.addAvgLifespan(0, initialData.get(0));
				dc.addAvgAgeAtDeath(0, w.calculateAvgAgeAtDeath());
				dc.addAvgIBI(0, initialData.get(1));
				dc.addAvgReproPeriod(0, initialData.get(2));
				dc.addAvgAgeFirstRepro(0, initialData.get(3));
				dc.addPopSize(0, w.persons.size());
				dc.addGrowthRate(0, 0.0);
				dc.addBirthRate(0, 0.0);
				dc.addDeathRate(0, 0.0);

				for(int i = 1; i <= 10000; i++) { //simulation runs for 5k years
					double pastPop = w.persons.size();
					double pastFemales = w.getAdultFemales();
					w.step();

					dc.increaseTimeStep();
					List<Double> data = w.collectLiveData();
					dc.addAvgLifespan(dc.getNumTimeSteps(), data.get(0));
					dc.addAvgAgeAtDeath(dc.getNumTimeSteps(), w.calculateAvgAgeAtDeath());
					dc.addAvgIBI(dc.getNumTimeSteps(), data.get(1));
					dc.addAvgReproPeriod(dc.getNumTimeSteps(), data.get(2));
					dc.addAvgAgeFirstRepro(dc.getNumTimeSteps(), data.get(3));
					dc.addPopSize(dc.getNumTimeSteps(), w.persons.size());
					double gr = (w.persons.size() - pastPop)/(pastPop * 0.5);
					dc.addGrowthRate(dc.getNumTimeSteps(), gr);
					dc.addBirthRate(dc.getNumTimeSteps(), w.numBirths/pastFemales);
					dc.addDeathRate(dc.getNumTimeSteps(), w.numDeaths/pastPop);

				}

				//writing results file
				String wb = "/Users/emilycoco/Desktop/Model Data Output/Growth Rate/";
				String name = "";
				if(n < 2) {
					name = "chimp";
				}
				else {
					name = "human";
				}
				if(w.toggleGrandmothers) {
					wb += "GMa/" + name + "_dc" + run + "_withGMa.csv";
				}
				else { 
					wb += "No GMa/" + name + "_dc" + run + "_noGMa.csv"; 
				}
				ExcelExport ee = new ExcelExport();
				ee.export(wb, dc, 0.5);

				System.out.println("end run");

			}

		}
	}

}
