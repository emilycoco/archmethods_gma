package archMethodsProject;

import java.io.*;
import java.util.List;

public class TestSmallPop {

	public static void main(String[] args) throws IOException {
		for(int run = 1; run <= 1; run++) {
			World w = new World(1000, Integer.MAX_VALUE, false, true, true);
			w.setInitialLifespan(29.44); //Gage 1998 value for Pan life expectancy at age 0 
			w.setInitialRP(40.515); //chimpanzee value from Gage 1998 and Gage 2001
			w.setFertLevel(0.0000276); //Gage 1998 chimpanzee value

			//w.setSilerModelParameters(0.175, 1.40, 0.00368, 0.000075); //values from Gage and Dyke 1986 females west
			//w.setSilerModelParameters(0.3, 1, 0.01, 0.0001); //values from Kachel et al
			//w.setSilerModelParameters(0.422, 1.1311, 0.013, 0.000147); //Gurven and Kaplan hunter-gatherer averages
			//w.setSilerModelParameters(0.351, 0.895, 0.0011, 0.0000067); //Hadza values from Gurven and Kaplan
			//w.setSilerModelParameters(2.864, 21.311, 0.01669, 0.00005260); //chimpanzee values from Gage 1998
			w.setSilerModelParameters(0.248, 0.608, 0.028, 0.00753);

			//w.setAgeDistributionPercentages(0.186, 0.135, 0.175, 0.116, 0.349, 0.039); //chimpanzee averages
			//w.setAgeDistributionPercentages(0.25, 0.19, 0.16, 0.12, 0.24, 0.04); //Kachel et al
			w.setAgeDistributionPercentages(0.21, 0.133, 0.162, 0.114, 0.3772, 0.038); //Hiraiwa-Hasegawa et al percentages
			
			w.initializePeople();
			System.out.println("BEGIN RUN");
			System.out.println(w.printWorld());
			
			DataCollector dc = new DataCollector();
			List<Double> initialData = w.collectLiveData();
			dc.addAvgLifespan(0, initialData.get(0));
			dc.addAvgAgeAtDeath(0, w.calculateAvgAgeAtDeath());
			dc.addAvgIBI(0, initialData.get(1));
			dc.addAvgReproPeriod(0, initialData.get(2));
			dc.addAvgAgeFirstRepro(0, initialData.get(3));
			dc.addPopSize(0, w.persons.size());
			dc.addGrowthRate(0, 0.0);
			dc.addBirthRate(0, 0.0);
			dc.addDeathRate(0, 0.0);

			//writing output to file
			String fileName = "/Users/emilycoco/Desktop/NYU/1st Year/Methods in Archaeology/Methods Paper - Demography and Agent-Based Modeling/Model Output";
			if(w.toggleGrandmothers) { fileName += "/GMa"; }
			else { fileName += "/No GMa"; }
			File f = new File(fileName, "output_" + run + ".txt");
			//File f = new File(fileName, "output.txt");
			FileOutputStream fs = new FileOutputStream(f);
			OutputStreamWriter osw = new OutputStreamWriter(fs);
			Writer writer = new BufferedWriter(osw);

			for(int i=1; i <= 10000 ; i++) {
				double pastPop = w.persons.size();
				double pastFemales = w.getAdultFemales();
				w.step();

				try {
					writer.write("STEP " + i + "\n");
					writer.write(w.getStepOutput() + "\n");
					writer.write("POPULATION SIZE: " + w.persons.size() + "\n");
					writer.write("\n");
				}
				catch (IOException e) {
					System.out.println("IO Exception");
					e.printStackTrace();
				}
				//if(i % 10 == 0) {
				dc.increaseTimeStep();
				List<Double> data = w.collectLiveData();
				dc.addAvgLifespan(dc.getNumTimeSteps(), data.get(0));
				dc.addAvgAgeAtDeath(dc.getNumTimeSteps(), w.calculateAvgAgeAtDeath());
				dc.addAvgIBI(dc.getNumTimeSteps(), data.get(1));
				dc.addAvgReproPeriod(dc.getNumTimeSteps(), data.get(2));
				dc.addAvgAgeFirstRepro(dc.getNumTimeSteps(), data.get(3));
				dc.addPopSize(dc.getNumTimeSteps(), w.persons.size());
				double gr = (w.persons.size() - pastPop)/(pastPop * 0.5);
				dc.addGrowthRate(dc.getNumTimeSteps(), gr);
				dc.addBirthRate(dc.getNumTimeSteps(), w.numBirths/pastFemales);
				dc.addDeathRate(dc.getNumTimeSteps(), w.numDeaths/pastPop);

				//}


			}
			writer.close();

			//writing results file
			String wb = "/Users/emilycoco/Desktop/Model Data Output/Test/";
			if(w.toggleGrandmothers) {
				wb += "GMa/dc" + run + "_withGMa.csv";
				//wb += "GMa/dc_withGMa.csv";
			}
			else { 
				wb += "No GMa/dc" + run + "_noGMa.csv"; 
				//wb+= "No GMa/dc_noGMa.csv";
			}
			ExcelExport ee = new ExcelExport();
			ee.export(wb, dc, 0.5);

			System.out.println("END RUN");
		}
	}

}
