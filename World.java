package archMethodsProject;

import java.util.*;


public class World {

	public int initNumIndiv;
	public int carryingCapacity;
	public double initLifespan, initRP;
	public double targetLifespan;

	public double c; //conception and delivery rate
	public double a1, b1, a2, a3, b3; //Siler model parameters
	public double inf, juv, ado, ya, pa, oa;

	public int ageOfWeaning;
	public double fertLevel; //total reproductive potential
	public double tradeoff;

	public List<Individual> persons;
	public List<Individual> newchildren;
	public List<Individual> dead;
	public List<Individual> adultMales;

	boolean toggleGrandmothers; //true means grandmothering can happen
	boolean toggleHetero; //true means heterogeneity is on
	boolean toggleMutations;
	boolean toggleOutput;
	boolean toggleDynamic;

	String stepOutput; 

	public int numBirths;
	public int numDeaths;

	public World(int num, int carrying, boolean grandmother, boolean h, boolean m) {
		stepOutput = "";

		ageOfWeaning = 2;
		initNumIndiv = num;
		carryingCapacity = carrying;


		persons = new ArrayList<Individual>();
		newchildren = new ArrayList<Individual>();
		adultMales = new ArrayList<Individual>(); 
		dead = new ArrayList<Individual>();

		toggleGrandmothers = grandmother;
		toggleHetero = h;
		toggleMutations = m;
		toggleOutput = false;
		toggleDynamic = false;

		numBirths = 0;
		numDeaths = 0;

	}

	public void setInitialLifespan(double d) {
		this.initLifespan = d;
	}
	public void setTargetLifespan(double d) {
		this.targetLifespan = d;
	}
	public void setTradeoff(double d) {
		this.tradeoff = d;
	}
	public void setInitialRP(double d) {
		this.initRP = d;
	}
	public void toggleOutput() {
		this.toggleOutput = true;
	}
	public void toggleDynamic() {
		this.toggleDynamic = true;
	}

	public String getStepOutput() { return stepOutput; }
	public void addOutput(String s) { stepOutput += s + "\n"; }
	public void clearOutput() { stepOutput = ""; }

	public void setFertLevel(double d) {
		this.fertLevel = d;
	}
	public void setSilerModelParameters(double a1, double b1, double a2, double a3, double b3) {
		this.a1 = a1; 
		this.b1 = b1; 
		this.a2 = a2;
		this.a3 = a3;
		this.b3 = b3;
	}

	public void setAgeDistributionPercentages (double inf, double juv, double ado, 
			double ya, double pa, double oa) {
		this.inf = inf;
		this.juv = juv;
		this.ado = ado;
		this.ya = ya;
		this.pa = pa;
		this.oa = oa;
	}

	public List<Integer> constructAgeDistribution (double inf, double juv, double ado, 
			double ya, double pa, double od) { 
		//scaling values from chimpanzee demography data, rounded up
		int infants = (int) (initNumIndiv * inf) + 1;
		int juveniles = (int) (initNumIndiv * juv) + 1;
		int adolescents = (int) (initNumIndiv * ado) + 1;
		int youngAdult = (int) (initNumIndiv * ya) + 1;
		int primeAdult = (int) (initNumIndiv * pa) + 1;
		int oldAdult = (int) (initNumIndiv * oa) + 1;

		List<Integer> ages = new ArrayList<Integer>();

		for(int i=0; i < infants; i++) {
			ages.add(generateRandomAge(0,4, new Random()));
		}
		for(int i=0; i < juveniles; i++) {
			ages.add(generateRandomAge(5,7, new Random()));
		}
		for(int i=0; i < adolescents; i++) {
			ages.add(generateRandomAge(8,15, new Random()));
		}
		for(int i=0; i < youngAdult; i++) {
			ages.add(generateRandomAge(16,20, new Random()));
		}
		for(int i=0; i < primeAdult; i++) {
			ages.add(generateRandomAge(21,40, new Random()));
		}
		for(int i=0; i < oldAdult; i++) {
			ages.add(generateRandomAge(41,75, new Random()));
		}

		return ages;	
	}

	public void initializePeople() {

		List<Integer> ages = constructAgeDistribution(inf, juv, ado, ya, pa, oa);

		for(int i = 1; i <= initNumIndiv; i++) {
			Individual newPerson = new Individual();

			double lifespan = initLifespan;
			if(toggleHetero) {
				lifespan = (initLifespan - 5.0) + (10.0) * new Random().nextDouble();
			}
			newPerson.setLifespan(lifespan);

			//set random age based on population composition like chimpanzees
			int ageIndex = generateRandomAge(0, ages.size()-1, new Random());
			newPerson.setAge(ages.get(ageIndex));
			ages.remove(ageIndex);

			newPerson.setSex(i % 2);

			newPerson.setAgeAtIndependence(newPerson.getLifespan()/10.0);

			double rp = initRP;
			if(toggleHetero) {
				rp = (initRP - 5.0) + (10.0) * new Random().nextDouble();
			}
			newPerson.setReproPeriod(rp);


			if(newPerson.getSex() == 0) {
				newPerson.setAgeAtSexMature(15);
				newPerson.setAgeAtEndFert(Math.min(2*newPerson.getLifespan(), 75));
			}
			else if(newPerson.getSex() == 1) {
				newPerson.setAgeAtSexMature(Math.min(15, ((newPerson.getLifespan()/4.0) + ageOfWeaning))); //based on chimpanzee ages at sexual maturity (Nishida et al 2003)
				newPerson.setAgeAtEndFert(newPerson.getAgeAtSexMature() + newPerson.getReproPeriod());
				newPerson.setAgeAtFrail(Math.min(2*newPerson.getLifespan(), 75));
			}

			if(newPerson.getAge() >= newPerson.getAgeAtSexMature() 
					&& newPerson.getAge() <= newPerson.getAgeAtEndFert()) {
				newPerson.setFertile(true);
			}


			newPerson.setWorld(this);

			persons.add(newPerson);
		}
		updateMalesList();
	}


	//method modified from http://www.javapractices.com/topic/TopicAction.do?Id=62
	private int generateRandomAge(int start, int end, Random random) {
		long range = (long)end - (long)start + 1;
		long fraction = (long)(range*random.nextDouble());
		int randomAge = (int)(fraction + start);
		return randomAge;
	}

	//method modified from http://www.javapractices.com/topic/TopicAction.do?Id=62
	private int getRandomIndex(Object[] indices, int start, int end, Random random) {
		long range = (long)end - (long)start + 1;
		long fraction = (long)(range*random.nextDouble());
		int randomIndex = (int)(fraction + start);

		while(indices[randomIndex] == null) {
			fraction = (long)(range*random.nextDouble());
			randomIndex = (int)(fraction + start);
		}
		return randomIndex;
	}

	public void updatePersonList() {
		for(Individual i : newchildren) {
			persons.add(i);
		}
		newchildren.clear();

		for(Individual i : dead) {
			int index = persons.indexOf(i);
			if(index != -1) {
				persons.remove(index);
			}
		}
		
		for(Individual i : persons) {
			i.setWorld(this);
		}
	}

	public void updateMalesList() {
		adultMales = new ArrayList<Individual>();
		for(Individual i : persons) {
			if(i.getSex() == 0) {
				if(i.getAge() >= i.getAgeAtSexMature()) {
					adultMales.add(i);
				}
			}
		}
	}

	public int getAdultFemales() {
		int count = 0;
		for(Individual i : persons) {
			if(i.getSex() == 1 && i.getAge() >= i.getAgeAtSexMature()) {
				count++;
			}
		}
		return count;
	}

	public void step() {
		clearOutput();
		newchildren.clear();
		dead.clear();
		adultMales.clear();
		updateMalesList();

		numBirths = 0;
		numDeaths = 0;

		int currentPop = persons.size() - 1;
		Individual [] people = new Individual[persons.size()];
		persons.toArray(people);

		for(int i = currentPop; i >= 0; i--) {
			int pIndex = getRandomIndex(people, 0, currentPop, new Random());
			Individual currentPerson = people[pIndex];
			people[pIndex] = null;

			if(currentPerson.isAlive()) {
				currentPerson.incrementAge();
				currentPerson.updateAge();
				currentPerson.checkNursing();

				if(currentPerson.getSex() == 1) {
					if(currentPerson.isFertile() && !currentPerson.isNursing()) {
						currentPerson.giveBirth();
					}
				}

				if(toggleGrandmothers) {
					if(currentPerson.getSex() == 1) {
						if(currentPerson.getAge() >= currentPerson.getAgeAtEndFert() 
								&& currentPerson.getAge() <= currentPerson.getAgeAtFrail()) {
							currentPerson.grandmother();
						}
					}
				}

				currentPerson.checkDeath();
			}

		}

		updatePersonList();

		popMortality();
		updatePersonList();
	}


	public void popMortality() {
		if(persons.size() > carryingCapacity) {
			if(toggleOutput) {
				addOutput("Population reached carrying capacity");
			}
			for(Individual i : persons) {
				double randomProb = Math.random();
				double popDeathProb = (double) (persons.size() - carryingCapacity)/(double) persons.size();

				if(i.getAge() != 0 ) { //babies who were just born can't die yet
					if(randomProb <= popDeathProb) {
						i.die();
					}
				}
			}
		}
	}

	public double collectiveMaleFert() {
		double maleFertility = 0;
		for(Individual m : adultMales) {
			if(m.isFertile()) {
				maleFertility += m.fertility();
			}
		}
		return maleFertility;
	}


	//---------- CALCULATION FOR DATA COLLECTION ----------------

	public List<Double> collectLiveData() {
		List<Double> data = new ArrayList<Double>();
		double lifespan = 0;
		double IBI = 0;
		int IBIcounter = 0;
		double reproPeriod = 0;
		double AFR = 0;
		int AFRcounter = 0;
		for(int i = 0; i < persons.size(); i++) {
			Individual p = persons.get(i);
			lifespan += Math.log(p.getLifespan());
			reproPeriod += Math.log(p.getReproPeriod());
			if(p.getSex() == 1 && p.isFertile() && p.getIBI() != 0) {
				IBI += Math.log(p.getIBI());
				IBIcounter++;
			}
			if(p.getSex() == 1) {
				double age = p.getAgeAtFirstRepro();
				if(age != 0) {
					AFR += Math.log(age);
					AFRcounter++;
				}
			}
		}
		data.add(Math.exp(lifespan /(double) persons.size()));
		data.add(Math.exp(IBI /(double) IBIcounter));
		data.add(Math.exp(reproPeriod /(double) persons.size()));
		data.add(Math.exp(AFR / (double) AFRcounter));

		return data;

	}

	public double getAvgLifespan() {
		double sum = 0;
		for(int i=0; i < persons.size(); i++) {
			sum += Math.log(persons.get(i).getLifespan());
		}
		return Math.exp(sum /(double) persons.size());
	}

	public double calculateAvgAgeAtDeath() {
		double sum = 0;
		//System.out.println(dead.size());
		for(int i = 0; i < dead.size(); i++) {
			//System.out.println(dead.get(i).getAgeAtDeath());
			sum += Math.log(dead.get(i).getAgeAtDeath());
		}
		return Math.exp(sum / (double) dead.size());
	}

	public double calculateAvgReproPeriod() {
		double sum = 0;
		for(int i = 0; i < persons.size(); i++) {
			sum += Math.log(persons.get(i).getReproPeriod());
		}
		return Math.exp(sum /(double) persons.size());
	}

	public String printWorld() {
		String output = "";
		for(Individual i : persons) {
			output += i.printIndividual() + "\n";
		}
		return output;
	}



}
